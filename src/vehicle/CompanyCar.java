/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicle;

/**
 *
 * @author Jessica
 */
public class CompanyCar extends Car  {
    
    //private final String usedBy;
    private final Employee E;
    
    public CompanyCar(String model, String year, String RegNum, String engineCapacity, int EmployeeNum, String EmployeeName) { 
        super(model,year,RegNum,engineCapacity);
        //super(year); incorrect 
        //super(RegNum); incorrect
        E = new Employee(EmployeeNum, EmployeeName);
        System.out.println("CompanyCar constructor called above"); 
    }
    
    @Override
    public String toString() {
        return super.toString() + " This Company Car is used by Employee = " + E.getEmployeeNum() + " Name = " + E.getEmployeeName() ;
   }
}

