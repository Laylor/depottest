/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicle;


abstract class Vehicle {
    
    protected String model;
    protected String year;
    protected String RegNum;
    
    //constructor ok for abstract class?
    //abstract classes constructors are generally used for super calls for initialization events common to all the subclasses
    public Vehicle(String model, String year, String RegNum) { 
        System.out.println("Vehicle constructor called"); 
        System.out.println("");
        this.model = model; 
        this.year = year; 
        this.RegNum = RegNum; 
    }
    
    //is it better to have toShow abstract and use public gettors and settors to show values or below?
    @Override
    public String toString(){
        return "Vehicle" + " Model = " + model + " Year = " + year + " RegNum = " + RegNum + " " ;
    }
    
}
