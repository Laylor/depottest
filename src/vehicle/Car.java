/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicle;

/**
 *
 * @author Jessica
 */
public class Car extends Vehicle {
    
    private final String engineCapacity;
    
    public Car(String model, String year, String RegNum, String engineCapacity) { 
        super(model,year,RegNum);
        //super(year); incorrect 
        //super(RegNum); incorrect
        this.engineCapacity = engineCapacity; 
        System.out.println("Car constructor called above"); 
    }
    
    @Override
    public String toString() {
        return super.toString() + "Engine Capacity = " + engineCapacity;
   }
}
