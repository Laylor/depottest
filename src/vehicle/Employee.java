/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicle;

/**
 *
 * @author Jessica
 */
public final class Employee {
    private int EmployeeNum;
    private String EmployeeName;
    
    public Employee(int EmployeeNum, String EmployeeName) { //should this be public
        //setEmployeeNum();
        this.EmployeeNum = EmployeeNum;
        this.EmployeeName = EmployeeName;
        
    }
    
    protected void setEmployeeNum(int EmployeeNum) {
        this.EmployeeNum = EmployeeNum;
    }
    
    public void setEmployeeName(String EmployeeName) {
        this.EmployeeName = EmployeeName;
    }
    
    public int getEmployeeNum() { 
        return EmployeeNum; 
    }
    
    public String getEmployeeName() { 
        return EmployeeName; 
    }
}
