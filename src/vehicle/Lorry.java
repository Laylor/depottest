/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicle;

/**
 *
 * @author Jessica
 */
public class Lorry extends Vehicle {
    
    private final String maximumLoad;
    
    public Lorry(String model, String year, String RegNum, String maximumLoad) { 
        super(model,year,RegNum);
        //super(year); incorrect 
        //super(RegNum); incorrect
        this.maximumLoad = maximumLoad; 
        System.out.println("Lorry constructor called above"); 
    }
    
    @Override
    public String toString() {
        return super.toString() + "Maximum Load = " + maximumLoad;
   }
}

