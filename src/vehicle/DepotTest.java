/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicle;

/**
 *
 * @author Jessica
 */
public class DepotTest {
    
    public static void main(String[] args){
        
        Vehicle [] vehicles = new Vehicle[4];
        //Employee [] employees = new Employee[4];
        
        vehicles [0] = new Car("FORD", "2020",  "ABC123" , "1LITRES");
        vehicles [1] = new Car("BMW", "2019",  "DEF456" , "2LITRES");
        vehicles [2] = new Lorry("Big Lorry", "2018",  "GHI789" , "25KG");
        vehicles [3] = new CompanyCar ("Porsche", "2020",  "Poshcar1" , "4LITRES", 001, "John Smith");
        for (Vehicle depot : vehicles){
            System.out.println(depot);
            if (depot instanceof Car){
                System.out.println("type is CAR!!!");
            } 
            else{
                System.out.println("NOT A CAR!!!");
            }
            System.out.println("");
                    //for ref
                    //Shape s1 = new Circle("Red", 2.2); 
                    //System.out.println(s1.toString()); 
        }
    }
}